#First Approach
#Bag of words for tweets


import csv
from afinn import sentiment
import pandas as pd
import numpy as np
import math
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn import cross_validation
from scipy.sparse import csr_matrix, vstack, hstack
from sklearn.metrics import make_scorer
from sklearn.svm import SVR
from sklearn.linear_model import SGDRegressor
from sklearn.metrics import mean_squared_error

#create custom RMSE scoring function
#scorer(estimator, X, y) format allows for scorer
#to be called by cross validation functions
def rmse(estimator, X, y):
	predictions = estimator.predict(X)
	return math.sqrt(np.mean((predictions - y)**2))

DIRECTORY = "/Users/Leland/Documents/Kaggle/kaggle-crowdflower"

#import data into dataframe
print "Importing data.\n"
train = pd.read_csv('data/train.csv',index_col='id',na_values=[''])
test = pd.read_csv('data/test.csv',index_col='id',na_values=[''])

#concatenate all text for semi-supervised learning
print "Concatening train and test tweets.\n"
all_text = pd.concat([train['tweet'],test['tweet']])

#put text into list
print "Training vectorizer.\n"
counter = CountVectorizer(stop_words = 'english',
	token_pattern = r'\w{1,}',
	ngram_range = (1,3), binary = False)

counter.fit(all_text)
counts = counter.transform(all_text)

#sentiment scores
print 'Creating sentiment scores.\n'
sentimentScore = lambda x: sentiment(x)
train['sentiment'] = train['tweet'].map(sentiment)
test['sentiment'] = test['tweet'].map(sentiment)

#convert sentiment scores to sparse matrices
#reshape arrays into vectors
print "Creating sparse matrices.\n"
train_sentiment = csr_matrix(np.array(train['sentiment']).reshape(len(train['sentiment']),1))
test_sentiment = csr_matrix(np.array(test['sentiment']).reshape(len(test['sentiment']),1))

#split into train and test again
train_counts = counts.tocsr()[:len(train.index)]
test_counts = counts.tocsr()[len(train.index):]

#create list of outcomes to predict
outcomes = list(train.columns[3:-1])

#create concatenated arrays of both sentiments and words
#must expliclty convert back to csr matrix for slicing operations
X_train = csr_matrix(hstack([train_counts, train_sentiment]))
X_test =csr_matrix(hstack([test_counts, test_sentiment]))

#create one model for each outcome:
models = []
errors = []
regularizer = [0.0001, 0.01,0.03,0.09,0.3,0.9,1,3,10]
eps = [0.0001, 0.01, 0.1, 0.3, 0.9, 1,3,9]

for y in outcomes:
	print "Training model for outcome: " + y
	min_score = float("inf")
	best_lambda = None
	best_classifier = None
	best_epsilon = None
	for l in regularizer:
		for e in eps:
			SVM = SGDRegressor(loss = 'epsilon_insensitive', 
				penalty = 'l2',
				alpha = l, n_iter = 10, shuffle = True,
				epsilon = e,
				verbose = False)
			scores = cross_validation.cross_val_score(
				estimator = SVM, 
				X = X_train, 
				y = train[y], cv = 5, 
				scoring = rmse,
				n_jobs = -1)
			score = scores.mean()
			sterr = scores.std() * 1.96
			if score < min_score:
				min_score = score
				best_classifier = SVM
				best_lambda = l
				best_epsilon = e
			print "  Current cost: %f  Epsilon: %f  RMSE: %f  RMSE St. Err.:%f" % (l,e,score,sterr)
	print "Best Cost: %f\tEpsilon: %f\tRMSE: %f\n" %(best_lambda, best_epsilon,min_score)
	best_classifier.fit(X_train, np.array(train[y]))
	models.append(best_classifier)
	errors.append((best_lambda, best_epsilon, min_score))

#estimate overall RMSE
print "Estimated Overall RMSE: %f\n" % (mean(map(lambda x: x[2], errors)))
print "Estimated Overall Std. Err.: %f\n" % (std(map(lambda x: x[2], errors)) * 1.96)

#apply model to test set
print "Applying optimal models.\n"
out = []
for i in range(X_test.shape[0]):
	preds = []
	for model in models:
		preds.append(float(model.predict(X_test[i,:])))
	out.append(preds)

#write predictions to file
print "Creating predictions file.\n"
identifiers = list(test.index)

with open('predictions.csv','wb') as f:
	writer = csv.writer(f)
	writer.writerow(['id'] + outcomes)
	for i in range(len(identifiers)):
		identifier = int(identifiers[i])
		predictions = out[i]
		#cap predictions at 0 and 1
		predictions = map(lambda x: 0 if x < 0 else 1 if x > 1 else x, predictions)
		writer.writerow([identifier] + predictions)

print "Processing complete."




